var xValues = ["GWK", "Bromo", "Kuta Lombok", "Labuan Bajo", "Jakarta"];
var yValues = [45, 50, 24, 40, 35];
var barColors = ["red", "green","blue","orange","brown"];

new Chart("myChart", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    title: {
      display: true,
      text: "Destination Statistic 2022"
    }
  }
});